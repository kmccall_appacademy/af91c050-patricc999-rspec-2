def reverser(&prc)
  if prc.call.split(' ').length == 1
    prc.call.reverse
  else
    prc.call.split(' ').map(&:reverse).join(' ')
  end
end

def adder(num = 1, &prc)
  prc.call + num
end

def repeater(num = 1, &prc)
  if num == 1
    prc.call
  else
    num.times { prc.call } 
  end
end
