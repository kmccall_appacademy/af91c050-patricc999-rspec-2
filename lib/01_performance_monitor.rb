def measure(num = 1, &prc)
  start_time = Time.now
  num.times { yield }
  time_elapsed = Time.now - start_time
  time_elapsed / num
end
